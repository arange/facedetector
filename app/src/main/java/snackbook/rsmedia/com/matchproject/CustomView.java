package snackbook.rsmedia.com.matchproject;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import org.opencv.core.Rect;


/**
 * Created by arange on 2017. 6. 19..
 */

public class CustomView extends View {
    private Rect[] mat = null;

    public CustomView(Context context) {
        super(context);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public void setMat(Rect[] m) {
        mat = m;
        ((Activity) getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        });
    }

    float resizeRatio = MainActivity.resizeRatio;

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mat == null) return;
        if (mat.length == 0) return;
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        for (int i = 0; i < mat.length; i++) {
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5.f);
            paint.setColor(Color.BLUE);

            double real_facesize_x = mat[i].x / resizeRatio;
            double real_facesize_y = mat[i].y / resizeRatio;
            double real_facesize_width = mat[i].width / resizeRatio;
            double real_facesize_height = mat[i].height / resizeRatio;
            android.graphics.Rect r = new android.graphics.Rect((int) real_facesize_x, (int) real_facesize_y,
                    (int) real_facesize_x + (int) real_facesize_width, (int) real_facesize_y + (int) real_facesize_height);
            canvas.drawRect(r, paint);
        }
    }
}
