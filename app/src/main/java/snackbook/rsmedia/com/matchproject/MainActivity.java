package snackbook.rsmedia.com.matchproject;

import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Size;
import org.opencv.features2d.DescriptorExtractor;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.FeatureDetector;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2 {

    public static float resizeRatio = 0.3f;

    public static native long loadCascade(String cascadeFileName);

    public static native void detect(long cascadeClassifier_face, long cascadeClassifier_eye, long matAddrInput, long matAddrResult, long faces);

    public static native void check(long matAddrResult, long cascadeClassifier_face, long faces);


    private CameraBridgeViewBase mOpenCvCameraView;
    private TextView textStatus;
    private Button btnChange;
    private CustomView customView = null;
    private Mat img_input;
    private int cameraId = 0;
    public long cascadeClassifier_face = 0;
    public long cascadeClassifier_eye = 0;
    public MatOfRect faces = new MatOfRect();

    FeatureDetector detector;
    DescriptorExtractor descriptor;
    DescriptorMatcher matcher;
    Mat descriptors2,descriptors1;
    Mat img1;
    MatOfKeyPoint keypoints1,keypoints2;

    private Object lock = new Object();

    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("native-lib");
    }

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    mOpenCvCameraView.enableView();
                    try {
                        initializeOpenCVDependencies();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags( WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_main);

        read_cascade_file();
        customView = (CustomView) findViewById(R.id.custom_view);
        textStatus = (TextView)findViewById(R.id.text);
        btnChange = (Button)findViewById(R.id.button_change);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.activity_surface_view);
        mOpenCvCameraView.setVisibility(View.VISIBLE);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setCameraIndex(0); // front-camera(1),  back-camera(0)

        mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);



        btnChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_3_2_0, this, mLoaderCallback);
        } else {
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        if(thread != null) {
            setThread();
            thread.start();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(thread != null) thread.interrupt();
    }

    public void onDestroy() {
        super.onDestroy();
        if (mOpenCvCameraView != null) {
            mOpenCvCameraView.disableView();
        }
    }

    private void initializeOpenCVDependencies() throws IOException {
        mOpenCvCameraView.enableView();
        detector = FeatureDetector.create(FeatureDetector.ORB);
        descriptor = DescriptorExtractor.create(DescriptorExtractor.ORB);
        matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
        img1 = new Mat();
        AssetManager assetManager = getAssets();
        InputStream istr = assetManager.open("abc.jpg");
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        Utils.bitmapToMat(bitmap, img1);
        Imgproc.cvtColor(img1, img1, Imgproc.COLOR_RGB2GRAY);
        img1.convertTo(img1, 0); //converting the image to match with the type of the cameras image
        descriptors1 = new Mat();
        keypoints1 = new MatOfKeyPoint();
        detector.detect(img1, keypoints1);
        descriptor.compute(img1, keypoints1, descriptors1);

    }

    private void read_cascade_file() {
        copyFile("haarcascade_frontalface_alt.xml");
        copyFile("haarcascade_eye_tree_eyeglasses.xml");

        cascadeClassifier_face = loadCascade("haarcascade_frontalface_alt.xml");
        cascadeClassifier_eye = loadCascade("haarcascade_eye_tree_eyeglasses.xml");
    }

    private void copyFile(String filename) {
        String baseDir = Environment.getExternalStorageDirectory().getPath();
        String pathDir = baseDir + File.separator + filename;

        AssetManager assetManager = this.getAssets();

        InputStream inputStream = null;
        OutputStream outputStream = null;

        try {
            inputStream = assetManager.open(filename);
            outputStream = new FileOutputStream(pathDir);

            byte[] buffer = new byte[1024];
            int read;
            while ((read = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, read);
            }
            inputStream.close();
            inputStream = null;
            outputStream.flush();
            outputStream.close();
            outputStream = null;
        } catch (Exception e) {

        }

    }


    Thread thread = null;

    private void setThread() {
        thread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        thread.sleep(500);

                        Mat img_result;
                        synchronized (lock) {
                            if (null == img_input) continue;
                            img_result = img_input.clone();
                        }
                        if (img_result == null) continue;

                        check(img_result.getNativeObjAddr(), cascadeClassifier_face, faces.getNativeObjAddr());
                        if(faces.toArray().length>0) {
                            setCheckMyFace(img_result);
                        }
                        img_result.release();

                        customView.setMat(faces.toArray());
                    }
                    catch (InterruptedException e) {
                        customView.setMat(faces.toArray());
                        e.printStackTrace();
                        continue;
                    }
                    catch (Exception e){
                        customView.setMat(faces.toArray());
                        thread.interrupt();
                        setThread();
                        thread.start();
                        return;
                    }
                }
            }
        });
    }



    private void setCheckMyFace(Mat result) throws Exception{
        Rect[] mat = faces.toArray();
        Mat img_result = result.clone();
        for(int i =0; i<mat.length; i++) {
            double real_facesize_x = mat[i].x / resizeRatio;
            double real_facesize_y = mat[i].y / resizeRatio;
            double real_facesize_width = mat[i].width / resizeRatio;
            double real_facesize_height = mat[i].height / resizeRatio;

            Rect rectCrop = new Rect((int) real_facesize_x, (int)real_facesize_y , (int)real_facesize_width, (int) real_facesize_height);
            Mat aInputFrame = new Mat();
            Imgproc.cvtColor(img_result.submat(rectCrop), aInputFrame, Imgproc.COLOR_RGB2GRAY);
            descriptors2 = new Mat();
            keypoints2 = new MatOfKeyPoint();
            detector.detect(aInputFrame, keypoints2);
            descriptor.compute(aInputFrame, keypoints2, descriptors2);

            // Matching
            MatOfDMatch matches = new MatOfDMatch();
            if (img1.type() == aInputFrame.type()) {
                matcher.match(descriptors1, descriptors2, matches);
            }
            List<DMatch> matchesList = matches.toList();

            Double max_dist = 0.0;
            Double min_dist = 100.0;

            for (int j = 0; j < matchesList.size(); j++) {
                Double dist = (double) matchesList.get(j).distance;
                if (dist < min_dist) min_dist = dist;
                if (dist > max_dist) max_dist = dist;
            }

            LinkedList<DMatch> good_matches = new LinkedList<>();
            for (int j = 0; j < matchesList.size(); j++) {
                if (matchesList.get(j).distance <= (1.5 * min_dist))
                    good_matches.addLast(matchesList.get(j));
            }

            if (good_matches.size() <= 7 && matchesList.size() > 200) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textStatus.setText("인증 성공!!!");
                    }
                });
                aInputFrame.release();
                break;
            }else{
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        textStatus.setText("인증 안됨.!!!");
                    }
                });
            }
            aInputFrame.release();
        }
        img_result.release();
    }

    @Override
    public void onCameraViewStarted(int width, int height) {
        float ratio = (float) width/ (float) height;
        mOpenCvCameraView.setMaxFrameSize(20, (int) (20 * ratio));
        setThread();
        thread.start();
    }

    @Override
    public void onCameraViewStopped() {

    }

    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
//        Mat input = inputFrame.rgba();
//        float ratio = (float) input.cols() / (float) input.rows();
//        img_input = new Mat(new Size(320 , 320 * ratio), CvType.CV_8UC4);
//        synchronized (lock) {
//            Imgproc.resize(input, img_input, new Size(320 , 320 * ratio));
//            Imgproc.resize(img_input, img_input, inputFrame.rgba().size());
//        }
        synchronized (lock){
            img_input = inputFrame.rgba();
        }
        return img_input;

//        if (img_result != null) img_result.release();
//        img_result = img_input.clone();
//        Log.e("arange","")
//        return img_result;
    }
}
