#include <jni.h>
#include <opencv2/opencv.hpp>
#include <android/asset_manager_jni.h>
#include <android/log.h>
#include <string>
#include<thread>



using namespace cv;
using namespace std;

extern "C" {
float resizeRatio = 0.3f;

JNIEXPORT void JNICALL
Java_snackbook_rsmedia_com_matchproject_MainActivity_check(JNIEnv *env, jclass type,
                                                           jlong matAddrResult,
                                                           jlong cascadeClassifier_face,
                                                           jlong faces) {

    // TODO
    vector<Rect> RectFaces;
    Mat &img_result = *(Mat *) matAddrResult;

    if(img_result.cols == 0 && img_result.rows ==0) return;

    Mat img_gray;

    cvtColor(img_result, img_gray, COLOR_BGR2GRAY);
    equalizeHist(img_gray, img_gray);

    Mat img_resize;

    resize(img_gray,img_resize, cv::Size(img_gray.cols*resizeRatio, img_gray.rows * resizeRatio),0,0,CV_INTER_NN);
        //-- Detect faces
    ((CascadeClassifier *) cascadeClassifier_face)->detectMultiScale( img_resize, RectFaces, 1.1, 2, 0|CASCADE_SCALE_IMAGE, Size(30, 30) );

    *((Mat*)faces) = Mat(RectFaces, true);

}


JNIEXPORT void JNICALL
Java_snackbook_rsmedia_com_matchproject_MainActivity_detect(JNIEnv *, jobject,
                                                                          jlong cascadeClassifier_face,
                                                                          jlong cascadeClassifier_eye,
                                                                          jlong addrInput,
                                                                          jlong addrResult,
                                                                            jlong faces) {

    Mat &img_input = *(Mat *) addrInput;
    Mat &img_result = *(Mat *) addrResult;

    img_result = img_input.clone();
//    for (int i = 0; i < RectFaces.size(); i++) {
//        double real_facesize_x = RectFaces[i].x / resizeRatio;
//        double real_facesize_y = RectFaces[i].y / resizeRatio;
//        double real_facesize_width = RectFaces[i].width / resizeRatio;
//        double real_facesize_height = RectFaces[i].height / resizeRatio;
//
//        Rect rect(real_facesize_x,real_facesize_y, real_facesize_width, real_facesize_height);
//        rectangle(img_result,rect,Scalar(0, 255, 0),5,8,0);
//
//        Rect face_area(real_facesize_x, real_facesize_y, real_facesize_width,real_facesize_height);
//        Mat faceROI = img_gray( face_area );
//        std::vector<Rect> eyes;
//
////        //-- In each face, detect eyes
////        ((CascadeClassifier *) cascadeClassifier_eye)->detectMultiScale( faceROI, eyes, 1.1, 2, 0 |CASCADE_SCALE_IMAGE, Size(30, 30) );
////
////        for ( size_t j = 0; j < eyes.size(); j++ )
////        {
////            Point eye_center( real_facesize_x + eyes[j].x + eyes[j].width/2, real_facesize_y + eyes[j].y + eyes[j].height/2 );
////            int radius = cvRound( (eyes[j].width + eyes[j].height)*0.25 );
////            circle( img_result, eye_center, radius, Scalar( 255, 0, 0 ), 30, 8, 0 );
////        }
//    }

}


JNIEXPORT jlong JNICALL
Java_snackbook_rsmedia_com_matchproject_MainActivity_loadCascade(JNIEnv *env, jobject,
                                                                               jstring cascadeFileName) {

    const char *nativeFileNameString = env->GetStringUTFChars(cascadeFileName, JNI_FALSE);

    string baseDir("/storage/emulated/0/");
    baseDir.append(nativeFileNameString);
    const char *pathDir = baseDir.c_str();

    jlong ret = 0;
    ret = (jlong) new CascadeClassifier(pathDir);
    if (((CascadeClassifier *) ret)->empty()) {
        __android_log_print(ANDROID_LOG_DEBUG, "native-lib :: ",
                            "CascadeClassifier로 로딩 실패  %s", nativeFileNameString);
    }
    else
        __android_log_print(ANDROID_LOG_DEBUG, "native-lib :: ",
                            "CascadeClassifier로 로딩 성공 %s", nativeFileNameString);

    return ret; }
}